package org.mazine.democounter.api;

import org.springframework.web.bind.annotation.*;

@RestController
public class HomeController {
    @GetMapping("/bonjour")
    public String getHome(@RequestParam(value = "fname") String firstName,
                          @RequestParam(value = "lname") String lastName) {
        return "Salut " + firstName + " " + lastName;
    }

    @PostMapping("/bonjour/{firstName}/{lastName}")
    public String postHome(@PathVariable String firstName,
                           @PathVariable String lastName) {
        return "Salut " + firstName + " " + lastName;
    }
}
