package org.mazine.democounter.api;

import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
public class CounterController {
    private int counter = 0;

    @GetMapping("/counter")
    public CounterDto getCounter() {
        CounterDto counterDto = new CounterDto();
        counterDto.counter = counter;

        return counterDto;
    }

    @PostMapping("/counter")
    public CounterDto postCounter(@RequestBody DeltaDto deltaDto) {
        counter += deltaDto.delta;

        return getCounter();
    }

    @PutMapping("/counter")
    public CounterDto putCounter(@RequestBody CounterDto counterDto) {
        counter = counterDto.counter;

        return getCounter();
    }

}
