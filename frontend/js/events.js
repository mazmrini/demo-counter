function increment() {
    _postDelta(1);
}

function decrement() {
    _postDelta(-1);
}

function updateValue(value) {
    counterDiv.textContent = value;
}

function getValue() {
    fetch("http://localhost:8080/counter")
       .then((response) => response.json())
       .then((data) => updateValue(data.counter));
}

function _postDelta(delta) {
    body = {
        "delta": delta
    };

    fetch("http://localhost:8080/counter",
         { method: "POST",
           headers: { "Content-Type": "application/json" },
           body: JSON.stringify(body)
         }).then((response) => response.json())
           .then((data) => updateValue(data.counter));
}