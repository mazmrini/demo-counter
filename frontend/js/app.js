function main() {
    incrementBtn.addEventListener('click', (event) => {
        increment();
    });

    decrementBtn.addEventListener('click', (event) => {
        decrement();
    });

    getValue();
}

document.addEventListener('DOMContentLoaded', (event) => {
  main();
});