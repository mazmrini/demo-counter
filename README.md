# demo-counter

## Run server

```
# compile
cd backend
mvnw.cmd package

# run
cd target
java -jar demo-original.<whatever>.jar
```

## Add static files to backend
1. Copy the `frontend` folder content into a `backend/src/main/resources/static` folder.
2. Compile the server
3. Run the server